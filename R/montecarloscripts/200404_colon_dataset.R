## colon dataset from survival


if (!require(survival, quietly = TRUE)) install.packages("survival")
if (!require(survminer, quietly = TRUE)) install.packages("survminer")
if (!require(dplyr, quietly = TRUE)) install.packages("dplyr")
if (!require(rlang, quietly = TRUE)) install.packages("rlang")

data("colon")
#head(colon)

#select only death as outcome event
cln <- filter(colon, etype==2)

#select only two arms of trial; obs and 5fu/lev
cln <- filter(cln, rx != "Lev")

cln_fit <- survfit(Surv(time, status) ~ rx, data = cln)



ggsurvplot(cln_fit,
           pval = TRUE, conf.int = FALSE,
           size = 1,
           risk.table = TRUE, # Add risk table
           break.time.by = 500,
           risk.table.col = "strata", # Change risk table color by groups
           linetype = "strata", # Change line type by groups
           #surv.median.line = "hv", # Specify median survival
           ggtheme = theme_classic(base_line_size = 1, base_rect_size = 2), # Change ggplot2 theme
           ncensor.plot = FALSE,
           censor = FALSE,
           palette = c("black", "black"))

res.cox <- coxph(Surv(time, status) ~ rx, data = cln)
res.cox

surv_diff <- survdiff(Surv(time, status) ~ rx, data = cln)
surv_diff

#calculate HR from this = (O1/E2)/(O2/E2)
cln_HR <- (surv_diff$obs[1]/surv_diff$exp[1])/(surv_diff$obs[2]/surv_diff$exp[2])
cln_HR_inv <- 1/cln_HR
cln_HR
cln_HR_inv


