script_file <- "C:/Users/AndrewFI/Dropbox/AFP/Systematic_review_project_Dec2018/Data_Analysis_Stats/Reverse_Engineer_KM_Plots/Point_Testing/colon/200404_cln_points_for_manual_extraction.xlsx"

sheet_name <- "60"

#number of patients in each arm
risk_t0_c <- 315
risk_t0_d <- 304

#chi_sq value to optimise for
chi_sq_value_to_optimise <- 9.97

#chi_sq_value_to_optimise<- qchisq(0.01, 1, lower.tail=FALSE)

#get_table_and_stats_hin(script_file, sheet_name, risk_t0_c, risk_t0_d, chi_sq_value_to_optimise)

get_table_and_stats(script_file, sheet_name, risk_t0_c, risk_t0_d, chi_sq_value_to_optimise)

