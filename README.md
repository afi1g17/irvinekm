# IrvineKM

### Andrew Comment

Have made various changes to the script as of 29/09/20.  These are mainly tidying of the user input section and other housekeeping.  See commits for more detail.

### Introduction
The Irvine method is a new method that uses non-linear optimisation to solve missing censor values and hazard ratios from the coordinates of Kaplan-Meier plots when this information is not provided in the primary source.

The Irvine method estimates summary statistics for the original plot 

### Installation
The easiest way to use this tool is to use the web-based Rshiny app available through shinyapps.io at:
https://edgreen21.shinyapps.io/km_hr/

For local installation from GitLab:  
`if (!require(devtools, quietly = TRUE)) install.packages("devtools")`<br/>
`if (!require(IrvineKM, quietly = TRUE)) install_gitlab('EdGreen21/IrvineKM')`

### Use
Instructions for using the `Irvine_method`  function for analysing Kaplan-Meier curves can be accessed in RStudio by typing `?Irvine_method`. The following arguments are required:  
  
  `time_vector` A vector of increasing time values, i.e. 'x' axis of Kaplain-Meier plot. First value must be '0'.  
`prob_c` A vector of decreasing survival values for control group, starting at either '100' as a percentage, or '1' as fraction.  
`prob_d` A vector of decreasing survival values for experimental group, starting at either '100' as a percentage, or '1' as fraction.  
`risk_t0_c` An integer value for the initial number of cases in the control group.  
`risk_t0_d` An integer value for the initial number of cases in the experimental group.  
`chi_sq_value_to_optimise` A numeric value for the chi-square value to optimise for  
`p_exact` A boolean value setting whether to treat the p value or chi-square value as exact or not. Defaults to TRUE.  
`known_risk` A vector describing known risk. Not currently used.  

### Example

Data derived from [ Hanley et al. J Natl Cancer Inst. 2018 Jan 1;110(1)](https://doi.org/10.1093/jnci/djx121) Figure 1a, middle panel

`Hanley_years          <- c(0.00, 0.13, 0.30, 0.49, 0.71, 0.97, 1.26, 1.85, 2.00, 2.63, 3.36, 3.63,3.96, 4.27, 4.68, 5.37, 6.74, 7.41, 8.02, 9.80)`  
`Hanley_control_p      <- c(100, 97.32, 92.61, 92.61, 87.26, 83.75, 79.22, 78.58, 78.58, 77.56, 73.41, 73.41, 70.91, 69.92, 69.92, 69.92, 66.90, 66.90, 66.90, 66.90)`  
`Hanley_experiment_p   <- c(100, 96.40, 94.04, 90.86, 84.00, 77.84, 75.55, 69.46, 68.70, 67.11, 59.00, 56.30, 56.30, 55.06, 55.06, 53.53, 51.34, 51.34, 46.17, 39.70)`   
`Hanley_control_n      <- 126`  
`Hanley_experiment_n   <- 146`  
`Hanley_p              <- 0.013`  
`Hanley_chi            <- qchisq(Hanley_p, df = 1, lower.tail=FALSE)`  
```
Result <- Irvine_method(time_vector              = Hanley_years,   
                        prob_c                   = Hanley_control_p,  
                        prob_d                   = Hanley_experiment_p,  
                        risk_t0_c                = Hanley_control_n,  
                        risk_t0_d                = Hanley_experiment_n,  
                        chi_sq_value_to_optimise = Hanley_chi,  
                        p_exact                  = TRUE)  

Result[["censor_values"]]  
Result[["hazard_ratio_values"]][["HR"]]
```
  

### Problems
If you have problems with the code, the best way to reach us is by raising an issue here in GitLab. You can also contact the authors, but email may well end up in spam:
Andrew Irvine, (afirvine at symbol gmail.com)
Edward Green, (e.green at symbol dkfz.de)

### References
paper currently under review
